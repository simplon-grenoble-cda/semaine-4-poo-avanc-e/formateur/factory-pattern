
# 1 - Problème

On veut un système qui nous permette de créer des instances de la classe `Actor`, avec un nom choisit aléatoirement
parmi une liste (Alice, Bob, Charlie, David, Eve).

Renseignez vous sur le design pattern "Factory" et mettez le en place pour résoudre ce problème.

# 2 - Static ou pas static

Suite à la solution que vous avez mis en place, posez vous la question : est ce que la méthode de création de votre factory
doit être static ou non ?

# 3 - Aller plus loin : factory paramétrable

On change légèrement notre problème : on veut un système que permette de créer des instances de la classe `Actor`, avec
un nom choisit aléatoirement, parmi une liste de noms dans une langue donnée. Nous avons une liste de noms américains
("Mary", "Jennifer", "Susan", "Ashley", "Dorothy"), et une liste de noms français ("Jean", "Louis", "Marie", "Pierre", "Camille").

Comment réutiliser le pattern Factory ? Avec les contraintes suivantes :

- Avoir une seule méthode de création
- Cette méthode doit pouvoir être appelée sans arguments

# 4 - Static ou pas static

Avec cette nouvelle solution, posez vous la question : est ce que la méthode de création de votre factory doit être static
ou non ?
